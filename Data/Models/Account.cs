﻿using System;

namespace Data.Models
{
    public class Account
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public DateTime RegistrationDate { get; set; }
        public decimal Amount { get; set; }
    }
}