﻿using System;

namespace Data.Models
{
    public class OperationHistory
    {
        public long Id { get; set; }
        public long Idaccaunt { get; set; }
        public DateTime OperationDate { get; set; }
        public int Idtype { get; set; }

        public decimal Amount { get; set; }
    }
}