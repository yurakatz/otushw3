﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public sealed class ATMDBContext
    {
        public List<Account> TableAccount { get; private set; }
        public List<OperationHistory> TableOperationHistory { get; private set; }
        public List<OperationTypes> TableOperationTypes { get; private set; }
        public List<Users> TableUsers { get;private  set; }

        public void Init()
        {
            var rand = new Random();
            var id = 0;

            /*Generate users*/

            TableUsers = new List<Users>();
            for (var i = 0; i < 5; i++)
                TableUsers.Add(new Users
                {
                    Id = i,
                    FirstName = $"FirstName{i}",
                    LastName = $"LastName{i}",
                    Login = $"Login{i}",
                    Password = $"Password{i}",
                    Patronymic = $"Patronymic{i}",
                    PassportId = $"PassportId{i}",
                    PhoneNumber = $"PhoneNumber{i}",
                    RegistrationDate = DateTime.Now.AddYears(-1).AddMonths(-i)
                });

            /*Generate Accounts*/

            TableAccount = new List<Account>();
            foreach (var t in TableUsers)
            {
                var countAccountants = rand.Next(1, 3);
                for (var j = 0; j < countAccountants; j++)
                    TableAccount.Add(new Account
                    {
                        Id = id++,
                        RegistrationDate = t.RegistrationDate.AddMonths(j),
                        UserId = t.Id,
                        Amount = rand.Next(-1000, 100000) 
                        
                    });
            }

            /*Generate Operation Types*/

            TableOperationTypes = new List<OperationTypes>
            {
                new OperationTypes {Id = 0, Type = "In"}, new OperationTypes {Id = 1, Type = "Out"}
            };

            /*Generate Operation History*/

            TableOperationHistory = new List<OperationHistory>();
            id = 0;
            foreach (var t in TableAccount)
            {
                var historyCounts = rand.Next(1, 5);
                for (var i = 0; i < historyCounts; i++)
                    TableOperationHistory.Add(new OperationHistory
                    {
                        Id = id++,
                        Idaccaunt = t.Id,
                        Idtype = rand.Next(1),
                        Amount = rand.Next(-100, 10000),
                        OperationDate =t.RegistrationDate.AddDays(i) 
                    });
            }
        }
    }
}