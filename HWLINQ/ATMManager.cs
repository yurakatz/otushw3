﻿using System;
using System.Collections.Generic;
using System.Linq;
using HWLINQ.Models;

namespace HWLINQ
{
    internal class ATMManager
    {
        private readonly ATMDBContext _context;

        public ATMManager(ATMDBContext context)
        {
            _context = context;
        }

        //Вывод информации о заданном аккаунте по логину и паролю
        public User GetUser(string login, string password)
        {
            return _context.TableUsers.FirstOrDefault(p => p.Login == login && p.Password == password);
        }

        //Вывод данных о всех счетах заданного пользователя
        public IEnumerable<Account> GetUserAccounts(User user)
        {
            if (user is null) throw new ArgumentNullException(nameof(user));

            return _context.TableAccount.Where(p => p.UserId == user.Id);
        }

        //Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта
        public IEnumerable<AccountsHistory> GetUserAccountsWithHistory(User user)
        {
            if (user is null) throw new ArgumentNullException(nameof(user));


            return GetUserAccounts(user).Select(p => new AccountsHistory
                {Account = p, History = _context.TableOperationHistory.Where(a => a.Idaccaunt == p.Id)}).ToList();
        }

        //Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта;
        public IEnumerable<UserHistoryCustom> GetAllInOperations()
        {
            return _context.TableUsers.Select(p => new UserHistoryCustom
            {
                User = p,
                OperationHistories = _context.TableOperationHistory.Where(h =>
                    h.OperationTypeIsIn && _context.TableAccount.Any(o => o.UserId == p.Id && h.Idaccaunt == o.Id)
                )
            });
        }

        // Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой);
        public IEnumerable<User> GetAllUsersWithAmountGreaterThan(decimal n)
        {
            return _context.TableUsers.Where(p =>
                _context.TableAccount.Any(h => h.Amount > n && h.UserId == p.Id));
        }
    }
}