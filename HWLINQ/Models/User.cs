﻿using System;

namespace HWLINQ.Models
{
    public class User
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string PhoneNumber { get; set; }
        public string PassportId { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public override string ToString()
        {
            return
                $"Id={Id} Login={Login} FirstName={FirstName} LastName={LastName} MiddleName={MiddleName} RegistrationDate={RegistrationDate}";
        }
    }
}