﻿using System;
using System.Collections.Generic;

namespace HWLINQ.Models
{
    public sealed class ATMDBContext
    {
        public List<Account> TableAccount { get; private set; }
        public List<OperationHistory> TableOperationHistory { get; private set; }

        public List<User> TableUsers { get; private set; }

        public void Init()
        {
            var id = 0;

            /*Generate users*/

            TableUsers = new List<User>();
            for (var i = 0; i < 5; i++)
                TableUsers.Add(new User
                {
                    Id = i,
                    FirstName = $"FirstName{i}",
                    LastName = $"LastName{i}",
                    Login = $"L{i}",
                    Password = $"P{i}",
                    MiddleName = $"MiddleName{i}",
                    PassportId = $"PassportId{i}",
                    PhoneNumber = $"PhoneNumber{i}",
                    RegistrationDate = DateTime.Now.AddYears(-1).AddMonths(-i)
                });

            /*Generate Accounts*/

            TableAccount = new List<Account>();
            foreach (var t in TableUsers)
                for (var j = 0; j < 2; j++)
                    TableAccount.Add(new Account
                    {
                        Id = id++,
                        RegistrationDate = t.RegistrationDate.AddMonths(j),
                        UserId = t.Id,
                        Amount = (j + 1) * 1000
                    });


            /*Generate Operation History*/

            TableOperationHistory = new List<OperationHistory>();
            id = 0;
            foreach (var t in TableAccount)
                for (var i = 0; i < 5; i++)
                    TableOperationHistory.Add(new OperationHistory
                    {
                        Id = id++,
                        Idaccaunt = t.Id,
                        OperationTypeIsIn = i % 2 == 0,
                        Amount = i * 100 + 100,
                        OperationDate = t.RegistrationDate.AddDays(i)
                    });
        }
    }
}