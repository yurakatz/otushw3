﻿using System;

namespace HWLINQ.Models
{
    public class Account
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public DateTime RegistrationDate { get; set; }
        public decimal Amount { get; set; }

        public override string ToString()
        {
            return $"Id={Id} UserId={UserId} RegistrationDate={RegistrationDate} Amount={Amount} ";
        }
    }
}