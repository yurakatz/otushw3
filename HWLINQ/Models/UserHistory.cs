﻿using System.Collections.Generic;

namespace HWLINQ.Models
{
    internal class UserHistoryCustom
    {
        public User User { get; set; }

        public IEnumerable<OperationHistory> OperationHistories { get; set; }
    }
}