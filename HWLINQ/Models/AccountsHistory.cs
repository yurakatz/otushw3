﻿using System.Collections.Generic;

namespace HWLINQ.Models
{
    internal class AccountsHistory
    {
        public Account Account { get; set; }

        public IEnumerable<OperationHistory> History { get; set; }
    }
}