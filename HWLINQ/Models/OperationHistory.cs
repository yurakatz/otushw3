﻿using System;

namespace HWLINQ.Models
{
    public class OperationHistory
    {
        public long Id { get; set; }
        public long Idaccaunt { get; set; }
        public DateTime OperationDate { get; set; }
        public bool OperationTypeIsIn { get; set; }

        public decimal Amount { get; set; }

        public override string ToString()
        {
            return
                $"Id={Id} OperationDate={OperationDate} OperationType={(OperationTypeIsIn ? "IN" : "OUT")} Amount={Amount} Idaccaunt={Idaccaunt}";
        }
    }
}