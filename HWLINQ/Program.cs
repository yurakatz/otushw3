﻿using System;
using System.Linq;
using HWLINQ.Models;

namespace HWLINQ
{
    internal class Program
    {
        private static void Main()
        {
            string inputValue;
            var dbContext = new ATMDBContext();
            dbContext.Init();
            var atmManager = new ATMManager(dbContext);


            do
            {
                DisplayAvailableOptions();
                inputValue = Console.ReadLine();
                switch (inputValue)
                {
                    case "0": break;
                    case "1":
                        ShowUser(atmManager);
                        break;
                    case "2":
                        ShowAllAccountsOfUser(atmManager);
                        break;
                    case "3":
                        ShowAllAccountsOfUserWithHistory(atmManager);
                        break;
                    case "4":
                        ShowAllInOperation(atmManager);
                        break;
                    case "5":
                        GetAllUsersWithAmountGreaterThan(atmManager);
                        break;
                }

                Console.WriteLine();
            } while (inputValue != "0");
        }

        private static void GetAllUsersWithAmountGreaterThan(ATMManager atmManager)
        {
            Console.WriteLine("Insert  N:");
            var input = Console.ReadLine();
            if (!decimal.TryParse(input, out var result))
            {
                Console.WriteLine("Invalid Value");
                return;
            }

            var val = atmManager.GetAllUsersWithAmountGreaterThan(result).ToList();
            if (!val.Any())
            {
                Console.WriteLine("No users to show");
                return;
            }

            foreach (var variable in val) Console.WriteLine(variable.ToString());
        }

        private static void ShowAllInOperation(ATMManager atmManager)
        {
            var val = atmManager.GetAllInOperations().ToList();
            if (!val.Any())
            {
                Console.WriteLine("No In operations to show");
                return;
            }

            foreach (var variable in val)
            {
                Console.WriteLine(variable.User);
                foreach (var variable2 in variable.OperationHistories) Console.WriteLine(variable2);
            }
        }

        private static void DisplayAvailableOptions()
        {
            Console.WriteLine("1. User Information");
            Console.WriteLine("2. Displaying Information on all accounts of  user");
            Console.WriteLine("3. Displaying Information on all accounts of  user,including history");
            Console.WriteLine("4. Displaying data on all account replenishment operations,include user information");
            Console.WriteLine("5. Displaying data about all users who have more than N in their account");
            Console.WriteLine("0. Exit");
            Console.WriteLine();
            Console.WriteLine("please select your choice:");
        }

        private static void ShowAllAccountsOfUserWithHistory(ATMManager atmManager)
        {
            var user = Login(atmManager);
            if (user is null)
            {
                Console.WriteLine("User Not Found");
                return;
            }

            var val = atmManager.GetUserAccountsWithHistory(user);
            foreach (var variable in val)
            {
                Console.WriteLine(variable.Account);
                foreach (var variable2 in variable.History)
                    Console.WriteLine(variable2.ToString());
            }
        }

        private static void ShowAllAccountsOfUser(ATMManager atmManager)
        {
            var user = Login(atmManager);

            if (user is null)
            {
                Console.WriteLine("User Not Found");
                return;
            }

            var userAccounts = atmManager.GetUserAccounts(user).ToList();

            if (!userAccounts.Any())
            {
                Console.WriteLine("no account found for this user");
                return;
            }

            foreach (var account in userAccounts) Console.WriteLine(account);
        }

        private static void ShowUser(ATMManager atmManager)
        {
            var user = Login(atmManager);
            Console.WriteLine(user == null ? "User Not Found" : user.ToString());
        }

        private static User Login(ATMManager atmManager)
        {
            Console.WriteLine("Insert User Login:");
            var login = Console.ReadLine();

            Console.WriteLine("Insert User Password:");
            var password = Console.ReadLine();

            return atmManager.GetUser(login, password);
        }
    }
}